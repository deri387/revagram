require("tools-for-instagram");

(async () => {
  console.log("\n -- Credit Deri Komara --\n".bold.underline);
  let ig = await login();
  setAntiBanMode(ig, true);
  let posts = await getUserRecentPosts(ig, "yunishara36");
  // console.log(posts[0].comments_disabled);
  let likers = await getRecentPostLikers(ig, posts[0]);
  if (Array.isArray(likers) && likers.length > 0) {
    let userLikers = likers;
    if (Array.isArray(userLikers) && userLikers.length > 0) {
      for (var i = 0; i < userLikers.length; i++) {
        (function (i) {
          setTimeout(async function () {
            if (userLikers[i].is_private) {
              const sendDm = await replyDirectMessage(
                ig,
                { userId: userLikers[i].pk },
                "Kantung mata parah? Bisa hilang dalam 2 menit di pemakaian pertama. Cek ig kita yuk @atasikantungmata.dalamduamenit ada PROMO menarik!!!"
              );
              console.log(sendDm);
              console.log("\nJumlah dm : " + parseInt(i + 1));
            } else {
              let postLikeRecent = await getUserRecentPosts(
                ig,
                userLikers[i].username
              );
              if (Array.isArray(postLikeRecent) && postLikeRecent.length > 0) {
                if (!postLikeRecent[0].comments_disabled) {
                  await commentPost(
                    ig,
                    postLikeRecent[0],
                    "Paten @atasikantungmata.dalamduamenit"
                  );
                  console.log("\nJumlah komen : " + parseInt(i + 1));
                }
              }
            }
          }, 120000 * i);
        })(i);
      }
    }
  }

  console.log("\nProcess done!\n".green);
})();
