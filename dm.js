require("tools-for-instagram");

(async () => {
  console.log("\n -- Credit Deri Komara --\n".bold.underline);
  let ig = await login();
  setAntiBanMode(ig, true);
  let posts = await getUserRecentPosts(ig, "lunamaya");
  let likers = await getRecentPostLikers(ig, posts[0]);
  if (Array.isArray(likers) && likers.length > 0) {
    let userLikers = likers.filter((v) => v.is_private === true);
    if (Array.isArray(userLikers) && userLikers.length > 0) {
      for (var i = 0; i < userLikers.length; i++) {
        (function (i) {
          setTimeout(async function () {
            const sendDm = await replyDirectMessage(
              ig,
              { userId: userLikers[i].pk },
              "Kantung mata parah? Bisa hilang dalam 2 menit di pemakaian pertama. Cek ig kita yuk @atasikantungmata.dalamduamenit ada PROMO menarik!!!"
            );
            console.log(sendDm);
            console.log("\nJumlah dm : " + parseInt(i + 1));
          }, 60000 * i);
        })(i);
      }
    }
  }

  console.log("\nProcess done!\n".green);
})();
